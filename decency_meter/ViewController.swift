//
//  ViewController.swift
//  decency_meter
//
//  Created by Владислав on 22.11.16.
//  Copyright © 2016 vladislav. All rights reserved.
//
import UIKit
import AVFoundation

class ViewController: UIViewController {

    @IBOutlet weak var gaugeView: GaugeView!
    @IBOutlet weak var label : UILabel!
    @IBOutlet weak var button : UIButton!
    
    var audioPlayer : AVAudioPlayer!
    
    var location : CGPoint = CGPointZero
    var levelOfDecency : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.label.text = "А помнишь, что такое порядочность? Я помню!"
        
        let geygerSound = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("geyger", ofType: "mp3")!)
        print(geygerSound)

        do {
            audioPlayer = try AVAudioPlayer(contentsOfURL: geygerSound)
        } catch {
            
        }
        audioPlayer.prepareToPlay()
        
        let touch = UITapGestureRecognizer(target:self, action: #selector(self.action(_:)))
        button.addGestureRecognizer(touch)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.gaugeView.backgroundColor = UIColor.clearColor()
        self.gaugeView.stops = [
            (0.0, UIColor.redColor()),
            (0.25, UIColor.orangeColor()),
            (0.5, UIColor.yellowColor()),
            (0.75, UIColor.greenColor())
        ]
        self.gaugeView.borderColor = UIColor.blackColor()
        
        self.setProgress(Double(0.0))
        let time = dispatch_time(dispatch_time_t(DISPATCH_TIME_NOW), 1 * Int64( NSEC_PER_SEC / 2 ))
        dispatch_after(time, dispatch_get_main_queue()) {
            self.setProgress(1.0)
            let time = dispatch_time(dispatch_time_t(DISPATCH_TIME_NOW), 1 * Int64( NSEC_PER_SEC / 2 ))
            dispatch_after(time, dispatch_get_main_queue()) {
                self.setProgress(0.0)
            }
        }
    }
    
    func setProgress(progress: Double) {
        self.gaugeView?.progress = progress
    }
    
    func action(recognizer : UITapGestureRecognizer) {
        location = recognizer.locationInView(self.button)
        print(location.x)
        
        let onePercent = button.bounds.width / 100
        let percent = location.x / onePercent
        print(percent)
        
        UIView.animateWithDuration(0.3) {
            self.audioPlayer.play()
            self.label.text = "Измеряем..."
            self.setProgress(0.93)
        
            let time = dispatch_time(dispatch_time_t(DISPATCH_TIME_NOW), 1 * Int64( NSEC_PER_SEC / 2 ))
            dispatch_after(time, dispatch_get_main_queue()) {
                self.setProgress(0.12)
                let time = dispatch_time(dispatch_time_t(DISPATCH_TIME_NOW), 1 * Int64( NSEC_PER_SEC / 2 ))
                dispatch_after(time, dispatch_get_main_queue()) {
                    self.setProgress(Double( percent/100 ))
                    let aStr = String(format: "%3.1f", percent)
                    self.label.text = "Уровень порядочности - \(aStr)%"
                    self.audioPlayer.stop()
                }
            }
        }
    }
}

